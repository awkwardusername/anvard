<?php namespace Atticmedia\Anvard;

use Cartalyst\Sentry\Facades\CI\Sentry;
use Hybrid_Auth;
use Hybrid_Provider_Adapter;
use Hybrid_User_Profile;
use Illuminate\Log\Writer;

class Anvard {

    /**
     * The configuration for Anvard
     *
     * @var array
     */
    protected $config;

    /**
     * The service used to login
     *
     * @var Hybrid_Provider_Adapter $adapter
     */
    protected $adapter;


    /**
     * The profile of the current user from
     * the provider, once logged in
     *
     * @var Hybrid_User_Profile
     */
    protected $adapter_profile;

    /**
     * The name of the current provider,
     * e.g. Facebook, LinkedIn, etc
     *
     * @var string
     */
    protected $provider;

    /**
     * The logger
     *
     * @var Writer
     */
    protected $logger;

    public function __construct($config) {
        $this->config = $config;
    }

    /**
     * Get a social profile for a user, optionally specifying
     * which social network to get, and which user to query
     */
    public function getProfile($network = NULL, $user = NULL) {
        if ( $user === NULL ) {
            $user = \Sentry::user();
            if (!$user) {
                return NULL;
            }
        }
        if ($network === NULL) {
            $profile = $user->profiles()->first();
        } else {
            $profile = $user->profiles()->where('network', $network)->first();
        }
        return $profile;
    }

    public function getProviders() {
        $haconfig = $this->config['hybridauth'];
        $providers = array();
        foreach ($haconfig['providers'] as $provider => $config) {
            if ( $config['enabled'] ) {
                $providers[] = $provider;
            }
        }
        return $providers;
    }


    /**
     * @return String
     */
    public function getCurrentProvider() {
        return $this->provider;
    }
    public function setCurrentProvider(String $provider) {
        $this->provider = $provider;
    }

    /**
     * @return Hybrid_Provider_Adapter
     */
    public function getAdapter() {
        return $this->adapter;
    }
    public function setAdapter(Hybrid_Provider_Adapter $adapter) {
        $this->adapter = $adapter;
    }


    /**
     * @return Writer
     */
    public function getLogger() {
        return $this->logger;
    }
    public function setLogger(Writer $logger) {
        $this->logger = $logger;
    }

    /**
     * Attempt a login with a given provider
     */
    public function attemptAuthentication($provider, Hybrid_Auth $hybridauth) {
        try {
            $this->provider = $provider;
            $adapter = $hybridauth->authenticate($provider);
            $this->setAdapter($adapter);
            $this->setAdapterProfile($adapter->getUserProfile());
            $profile = $this->findProfile();
            return $profile;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function setAdapterProfile(Hybrid_User_Profile $profile) {
        $this->adapter_profile = $profile;
    }

    /**
     * @return Hybrid_User_Profile
     */
    public function getAdapterProfile() {
        return $this->adapter_profile;
    }

    protected function findProfile() {
        $adapter_profile = $this->getAdapterProfile();
        $ProfileModel = $this->config['db']['profilemodel'];
        $UserModel = $this->config['db']['usermodel'];
        $user = NULL;

        // Have they logged in with this provider before?
        $profile_builder = call_user_func_array(
            "$ProfileModel::where",
            array('provider', $this->provider)
        );
        $profile = $profile_builder
            ->where('identifier', $adapter_profile->identifier)
            ->first();

        \Log::info($profile);

        /**
         * Check if user is currently logged in first
         *    ... then check if their is a profile matching the social user
         *    ... then check if the email matches
         * If none of the above, create a new user.
         */
        if (\Sentry::check()) {
            $user = \Sentry::getUser();
        } elseif ($profile) {
            // ok, we found an existing user
            $user = $profile->user()->first();
            $this->logger->debug('Anvard: found a profile, id='.$profile->id);
        } elseif ($adapter_profile->email) {
            $this->logger->debug('Anvard: could not find profile, looking for email');
            // ok it's a new profile ... can we find the user by email?
            $user_builder = call_user_func_array(
                "$UserModel::where",
                array('email', $adapter_profile->email)
            );
            $user = $user_builder
                ->first();
        }
        // If we haven't found a user, we need to create a new one
        if (!$user) {
            $this->logger->debug('Anvard: did not find user, creating');
            //$user = new $UserModel();
            $user = \Sentry::getUserProvider()->getEmptyUser();
            // map in anything from the profile that we want in the User
            $map = $this->config['db']['profiletousermap'];
            foreach ($map as $apkey => $ukey) {
                $user->$ukey = $adapter_profile->$apkey;
            }

            // Default username,email,password/confirmation
            $user->username = $adapter_profile->identifier;
            if(!empty($adapter_profile->emailVerified))
                $user->email = $adapter_profile->emailVerified;
            elseif(!empty($adapter_profile->email))
                $user->email = $adapter_profile->email;
            else
                $user->email = $adapter_profile->identifier . '@changeyouremail.now';
            $user->password = uniqid();
            $user->activated = 1;

            // get the custom config from the db.php config file
            $values = $this->config['db']['uservalues'];
            foreach ( $values as $key=>$value ) {
                if (is_callable($value)) {
                    $user->$key = $value($user, $adapter_profile);
                } else {
                    $user->$key = $value;
                }
            }

            $rules = $this->config['db']['userrules'];
            $result = $user->save($rules);

            $user->addGroup(\Sentry::findGroupById(3));

            if ( !$result ) {
                $this->logger->error('Anvard: FAILED TO SAVE USER');
                return NULL;
            }
        } 
        if (!$profile) {
            // If we didn't find the profile, we need to create a new one
            $profile = $this->createProfileFromAdapterProfile($adapter_profile, $user);
        } else {
            // If we did find a profile, make sure we update any changes to the source
            $profile = $this->applyAdapterProfileToExistingProfile($adapter_profile, $profile);
        }
        $result = $profile->save();

        try {
            \Customer::whereUserId($profile->user_id)->firstOrFail();
        } catch (\Exception $ex) {
            if($this->createCustomerProfile($profile)) {
                $result = true;
            }
        }

        if (!$result) {
            $this->logger->error('Anvard: FAILED TO SAVE PROFILE');
            return NULL;
        }
        $this->logger->info('Anvard: succesful login!');
        return $profile;

    }

    protected function createCustomerProfile($profile) {
        $location = $profile->region;
        $source_ip = $_SERVER['REMOTE_ADDR'];

        $location_ = \Customer::resolveLocation($location, $source_ip);

        $customerDetail = [
            'first_name'    => $profile->firstName,
            'last_name'     => $profile->lastName,
            'location'      => $location_,
            'budget'        => 0,
            'wedding_date'  => '2014-01-01',
            'user_id'       => $profile->user_id,
            'avatar'        => null,
            'wedding_stime' => 120000,
            'wedding_etime' => 120000,
            'about'         => substr($profile->description,0,512),
            'no_of_guests'  => 0,
            'is_new_user'   => 1,
            'is_public'     => 1
        ];

        try {
            $customer = \Customer::create($customerDetail);
            $this->_fillProfileQuestions($customer);    
        } catch (Exception $e) {
            return false;            
        }

        return true;
    }

    protected function createProfileFromAdapterProfile($adapter_profile, $user) {
        $ProfileModel = $this->config['db']['profilemodel'];
        $attributes['provider'] = $this->provider;
        // @todo use config value for foreign key name
        $attributes['user_id'] = $user->id;
        $profile = new $ProfileModel($attributes);
        $profile = $this->applyAdapterProfileToExistingProfile($adapter_profile, $profile);
        return $profile;
    }

    protected function applyAdapterProfileToExistingProfile($adapter_profile, $profile) {
        $attributes = get_object_vars($adapter_profile);
        foreach ($attributes as $k=>$v) {
            $profile->$k = $v;
        }
        return $profile;
    }

    public static function _fillProfileQuestions(\Customer $customer = null) {
        $questions = \Question::whereIsRecommended(1)->get();

        $questions->each(function ($question) use ($customer) {
            \CustomerQuestion::create([
                'question_id' => $question->id,
                'category_id' => $question->category_id,
                'customer_id' => $customer->id
            ]);
        });
    }
}